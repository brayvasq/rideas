# Rideas
## Setup project
Create a simple rails project
```bash
rails new rideas
```

Run project
```bash
rails server
```

## Ideas Engines
Create a new engine
```bash
rails plugin new todo --mountable
```

Move engine to engine folder
```bash
mkdir engines && mv ideas ./engines
```

Add engine to `Gemfile`.
```ruby
# Engines
gem 'ideas', path: 'engines/ideas'
```

Mount engine in `config/routes.rb`
```ruby
Rails.application.routes.draw do
  mount Todo::Engine => "/", as: 'todo'
end
```

Expand migrations
```ruby
# engines/ideas/lib/ideas/engine.rb
module Ideas
  class Engine < ::Rails::Engine
    isolate_namespace Ideas

    initializer :append_migrations do |app|
      unless app.root.to_s.match(root.to_s)
        config.paths['db/migrate'].expanded.each do |p|
          app.config.paths['db/migrate'] << p
        end
      end
    end
  end
end
```

Create scaffold app
```bash
cd engines/todo && rails g scaffold Task title:string content:text
```

Migrate
```bash
rails db:migrate
```

Routes
```ruby
# engines/ideas/config/routes.rb
Ideas::Engine.routes.draw do
  resources :items
  root 'items#index'
end
```

Engine webpacker: 
- https://github.com/rails/rails/issues/37656
- https://github.com/rails/webpacker/issues/348
- https://stackoverflow.com/questions/35990897/rails-engine-assests-are-not-precompiled
