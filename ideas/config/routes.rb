Ideas::Engine.routes.draw do
  resources :items
  root 'items#index'
end
