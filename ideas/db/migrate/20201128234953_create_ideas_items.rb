class CreateIdeasItems < ActiveRecord::Migration[6.0]
  def change
    create_table :ideas_items do |t|
      t.string :title
      t.text :content

      t.timestamps
    end
  end
end
