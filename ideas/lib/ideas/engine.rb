# frozen_string_literal: true

module Ideas
  class Engine < ::Rails::Engine
    isolate_namespace Ideas

    initializer :append_migrations do |app|
      unless app.root.to_s.match(root.to_s)
        config.paths['db/migrate'].expanded.each do |p|
          app.config.paths['db/migrate'] << p
        end
      end
    end

    initializer 'engine_name.assets.precompile' do |app|
      app.config.assets.precompile += ['*.js', '*.css', '**/*.js', '**/*.css', '*.jpg',
                                       '*.png', '*.ico', '*.gif', '*.woff2', '*.eot',
                                       '*.woff', '*.ttf', '*.svg']
    end
  end
end
